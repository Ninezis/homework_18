#include <iostream>
#include <vector>
#include <string>

using namespace std;
template<typename S>

class Stack
{
public:
    void push(S);
    S pop();
    void show();
private:
    vector<S> v;
};

template<class S> void Stack<S>::push(S elem)
{
    v.push_back(elem);
}

template<class S> S Stack<S>::pop()
{
    S elem = v.back();
    v.pop_back();
    return elem;
}
template<class S> void Stack<S>::show()
{
    cout << "stack: ";
    for (auto e : v) cout << e << " ";
    cout << endl;
}



int main() {
    Stack<double> a;
    a.push(1.1);  a.push(0.2);  a.push(3);
    a.show();
    cout << "poped: " << a.pop() << endl;
    cout << endl;
    a.show();
    cout << endl;

    Stack<string> b;
    b.push("Elden"); b.push("ring");
    b.show();
    cout << "poped: " << b.pop() << endl;
    cout << endl;
    b.show();

    return 0;
}


